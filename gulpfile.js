const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const pump = require('pump');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

// Clean task

gulp.task('clean-dist', function(){
    return gulp.src('./dist', {read: false})
        .pipe(clean());
});

// HTML tasks

gulp.task('copy-html', function(){
    return gulp.src('./src/**/*.html')
               .pipe(gulp.dest('./dist'));
});

// CSS tasks

gulp.task('sass', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('autoprefixer', ['sass'], function(){
    gulp.src('./src/css/**/*.css')
        .pipe(autoprefixer({
            browsers: [' > 0.1%'],
            cascade: false
        }))
        .pipe(gulp.dest('src'))
});

gulp.task('concat-css', ['autoprefixer'],function() {
    return gulp.src(['./src/css/**/*.css','!./src/css/all.css'])
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./src/css/'));
});

gulp.task('compress-css', ['concat-css'], function() {
    return gulp.src('./src/css/all.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename("/all.min.css"))
        .pipe(gulp.dest('./src/css'));
});

gulp.task('copy-css', ['compress-css'], function() {
    return gulp.src('./src/css/all.min.css')
        .pipe(gulp.dest('./dist/css'));
});

// TODO add autoprefixer, concat, compress, copy-css


// JS tasks



gulp.task('concat-js', function() {
    return gulp.src(['./src/js/**/*.js','!./src/js/all.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./src/js/'));
});

gulp.task('transpile', ['concat-js'], () =>
    gulp.src('./src/js/all.js')
        .pipe(babel({presets: ["@babel/env"]}))
        .pipe(gulp.dest('./src/js'))
);

gulp.task('compress', ['transpile'], function (cb) {
    pump([
            gulp.src('./src/js/all.js'),
            uglify(),
            rename("/all.min.js"),
            gulp.dest('src/js/')
        ],
        cb
    );
});

gulp.task('copy-js', ['compress'], function(){
    return gulp.src('./src/js/all.min.js')
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('serve', function(){
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/**/*.html',['copy-html']).on('change',browserSync.reload);
    gulp.watch('./src/scss/**/*.scss',['copy-css']).on('change',browserSync.reload);
    gulp.watch('./src/js/**/*.js',['copy-js']).on('change',browserSync.reload);
});

gulp.task('dev', gulpSequence('clean-dist', ['copy-html','copy-js','copy-css'], 'serve'));